// @flow
import * as React from 'react';
import Link from 'next/link';

type Props = {
    children?: React.Node,
    href: string,
    type?: string
};

const LinkType = ({href = '', children, type = 'button', ...props}: Props) => {
    if (href.includes('http') || href.includes('mailto:') || href.includes('tel:')) {
        return (
            <a href={href} target="noopener" {...props}>
                {children}
            </a>
        );
    } else if (href) {
        return (
            <Link href={href}>
                <a {...props}>{children}</a>
            </Link>
        );
    } else {
        return (
            <button type={type} {...props}>
                {children}
            </button>
        );
    }
};

export default LinkType;
