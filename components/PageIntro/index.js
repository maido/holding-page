// @flow
import * as React from 'react';
import {useSpring, animated} from 'react-spring/web.cjs';
import Logo from '../Logo';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    children?: React.Node,
    image?: string,
    primaryCTA: CTA,
    secondaryCTA: CTA,
    subtitle?: string,
    text?: string,
    title?: string
};

const PageIntro = ({children, image, primaryCTA, secondaryCTA, subtitle, text, title}: Props) => {
    const config = {
        config: {tension: 100, mass: 2, friction: 20},
        from: {transform: `translateY(30px)`, opacity: 0},
        to: {transform: `translateY(0)`, opacity: 1}
    };
    const badgeAnimation = useSpring({
        config: {tension: 200, mass: 4, friction: 30},
        from: {transform: `scale(0)`, opacity: 0},
        to: {transform: `scale(1)`, opacity: 1}
    });
    const subtitleAnimation = useSpring({...config, delay: 500});
    const titleAnimation = useSpring({...config, delay: 700});
    const textAnimation = useSpring({...config, delay: 1000});

    return (
        <S.Container>
            {image && (
                <animated.div style={badgeAnimation}>
                    <Logo />
                    <UI.Spacer />
                </animated.div>
            )}
            {subtitle && (
                <animated.div style={subtitleAnimation}>
                    <UI.Subheading>{subtitle}</UI.Subheading>
                </animated.div>
            )}
            {title && (
                <animated.div style={titleAnimation}>
                    <UI.Heading1>{title}</UI.Heading1>
                </animated.div>
            )}
            {text && (
                <animated.div style={textAnimation} dangerouslySetInnerHTML={{__html: text}} />
            )}
            {children && (
                <React.Fragment>
                    <UI.Spacer size="s" />
                    {children}
                </React.Fragment>
            )}
        </S.Container>
    );
};

export default PageIntro;
