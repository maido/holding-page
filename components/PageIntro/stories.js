// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, object, select, text} from '@storybook/addon-knobs';
import PageIntro from './';
import Theme from '../Theme';

const themeOptions = {
    Primary: 'primary',
    Secondary: 'secondary',
    Tertiary: 'tertiary',
    White: 'white'
};
const stories = storiesOf('PageIntro', module);

stories.addDecorator(withKnobs);

stories.addDecorator(story => (
    <Theme theme={select('Theme', themeOptions, 'secondary')}>{story()}</Theme>
));

stories.add('default', () => {
    return (
        <PageIntro
            title="Foo bar"
            text="Excepteur cupidatat fugiat ut excepteur enim velit qui veniam mollit deserunt sit. Ullamco commodo qui magna aliqua excepteur in tempor dolor et sint eu commodo ad. Qui amet est sit magna sunt voluptate consectetur sit."
        />
    );
});

stories.add('with subtitle', () => {
    return (
        <PageIntro
            title="Foo bar"
            subtitle="Foo bar baz"
            text="Excepteur cupidatat fugiat ut excepteur enim velit qui veniam mollit deserunt sit. Ullamco commodo qui magna aliqua excepteur in tempor dolor et sint eu commodo ad. Qui amet est sit magna sunt voluptate consectetur sit."
        />
    );
});

stories.add('with CTAs', () => {
    return (
        <PageIntro
            title="Foo bar"
            text="Excepteur cupidatat fugiat ut excepteur enim velit qui veniam mollit deserunt sit. Ullamco commodo qui magna aliqua excepteur in tempor dolor et sint eu commodo ad. Qui amet est sit magna sunt voluptate consectetur sit."
            primaryCTA={{label: 'Foo', url: '/'}}
            secondaryCTA={{label: 'Foo', url: '/'}}
        />
    );
});

stories.add('playground', () => {
    return (
        <PageIntro
            title={text('Title', 'Foo bar')}
            text={text(
                'Text',
                'Excepteur cupidatat fugiat ut excepteur enim velit qui veniam mollit deserunt sit. Ullamco commodo qui magna aliqua excepteur in tempor dolor et sint eu commodo ad. Qui amet est sit magna sunt voluptate consectetur sit.'
            )}
            primaryCTA={object('Primary CTA', {label: 'Foo', url: '/'})}
            secondaryCTA={object('Secondary CTA', {label: 'Foo', url: '/'})}
        />
    );
});
