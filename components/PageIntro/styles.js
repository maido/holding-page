// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, spacing} from '../../globals/variables';

export const Container = styled.section`
    font-size: ${rem(18)};
    line-height: 1.6;
    margin-left: auto;
    margin-right: auto;
    max-width: ${rem(650)};
    padding-left: ${rem(spacing.m)};
    padding-right: ${rem(spacing.m)};
    padding-bottom: ${rem(spacing.m)};

    text-align: center;

    @media (min-width: ${rem(breakpoints.tablet)}) {
        font-size: ${rem(20)};
    }
`;

export const image = css`
    height: ${rem(100)};
    width: ${rem(100)};
`;
