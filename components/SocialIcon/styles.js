// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {colors, spacing} from '../../globals/variables';

const SIZE = 34;

export const Container = styled.a`
    align-items: center;
    background-color: ${colors.white};
    border-radius: 100%;
    height: ${rem(SIZE)};
    display: flex;
    justify-content: center;
    overflow: hidden;
    padding: ${rem(spacing.xs)};
    width: ${rem(SIZE)};

    &:hover,
    &:focus {
        transform: scale(1.1);
    }
`;

export const Svg = styled.svg`
    height: auto;
    max-height: ${rem(SIZE * 0.6)};
    width: ${rem(SIZE * 0.6)};

    path {
        fill: ${colors.purplishBlue};
    }
`;
