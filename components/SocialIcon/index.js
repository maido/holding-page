// @flow
import React from 'react';
import * as S from './styles';

type Props = {
    site: string
};

export const siteUrls = {
    twitter: 'https://twitter.com/thoughtforfood_',
    facebook: 'https://www.facebook.com/thoughtforfoodorg/',
    instagram: 'https://www.instagram.com/thoughtforfoodorg/',
    linkedIn: 'https://www.linkedin.com/company/thoughtforfood/',
    vimeo: 'https://vimeo.com/thoughtforfoodorg'
};

const SocialIcon = ({site}: Props) => {
    if (site === 'twitter') {
        return (
            <S.Container href={siteUrls.twitter} target="noopener" title="Follow us">
                <S.Svg
                    width="16"
                    height="14"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 16 14"
                >
                    <path
                        d="M16 2.038c-.59.26-1.22.437-1.885.517.677-.407 1.198-1.05 1.443-1.816-.634.37-1.337.64-2.085.79-.598-.64-1.45-1.04-2.396-1.04-1.812 0-3.282 1.47-3.282 3.28 0 .26.03.51.085.75-2.728-.13-5.147-1.44-6.766-3.42C.83 1.58.67 2.14.67 2.75c0 1.14.58 2.143 1.46 2.732-.538-.017-1.045-.165-1.487-.41v.04c0 1.59 1.13 2.918 2.633 3.22-.276.074-.566.114-.865.114-.21 0-.41-.02-.61-.058.42 1.304 1.63 2.253 3.07 2.28-1.12.88-2.54 1.404-4.07 1.404-.26 0-.52-.015-.78-.045 1.46.93 3.18 1.474 5.04 1.474 6.04 0 9.34-5 9.34-9.33 0-.14 0-.28-.01-.42.64-.46 1.2-1.04 1.64-1.7L16 2.038z"
                        fillRule="evenodd"
                    />
                </S.Svg>
            </S.Container>
        );
    } else if (site === 'facebook') {
        return (
            <S.Container href={siteUrls.facebook} target="noopener" title="Follow us">
                <S.Svg width="8" height="14" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 8 14">
                    <path
                        d="M2.6279 13.21143v-5.847H.6594V5.0864h1.9685V3.40383C2.6279 1.4542 3.8188.39351 5.55895.39351c.83326 0 1.54951.0604 1.75806.0887v2.03834h-1.2079c-.94367 0-1.12769.45297-1.12769 1.11354v1.45325H7.2368l-.29254 2.2837H4.98142v5.84039H2.6279z"
                        fillRule="evenodd"
                    />
                </S.Svg>
            </S.Container>
        );
    } else if (site === 'instagram') {
        return (
            <S.Container href={siteUrls.instagram} target="noopener" title="Follow us">
                <S.Svg
                    height="24"
                    width="24"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 24 24"
                >
                    <path d="M15.5 3h-7A5.5 5.5 0 0 0 3 8.5v7A5.5 5.5 0 0 0 8.5 21h7a5.5 5.5 0 0 0 5.5-5.5v-7A5.5 5.5 0 0 0 15.5 3zm4 12.5a4.00454 4.00454 0 0 1-4 4h-7a4.00454 4.00454 0 0 1-4-4v-7a4.0045 4.0045 0 0 1 4-4h7a4.0045 4.0045 0 0 1 4 4z" />
                    <path d="M12 7.5a4.5 4.5 0 1 0 4.5 4.5A4.5 4.5 0 0 0 12 7.5zm0 7.5a3 3 0 1 1 3-3 3.0034 3.0034 0 0 1-3 3z" />
                    <circle cx="16.70001" cy="7.29999" r="1" />
                </S.Svg>
            </S.Container>
        );
    } else if (site === 'linkedIn') {
        return (
            <S.Container href={siteUrls.linkedIn} target="noopener" title="Follow us">
                <S.Svg
                    width="24"
                    height="24"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 24 24"
                >
                    <path
                        d="M20.448 20.4525h-3.555V14.883c0-1.329-.027-3.0375-1.851-3.0375-1.8525 0-2.136 1.446-2.136 2.94v5.667H9.351V9h3.414v1.56h.045c.477-.9 1.638-1.8495 3.3705-1.8495 3.6 0 4.2675 2.37 4.2675 5.4555v6.2865zM5.337 7.4325c-1.143 0-2.064-.9255-2.064-2.0655 0-1.137.921-2.0625 2.064-2.0625 1.14 0 2.064.9255 2.064 2.0625 0 1.14-.9255 2.0655-2.064 2.0655zm1.782 13.02H3.555V9h3.564v11.4525zM22.224 0H1.77C.792 0 0 .774 0 1.7295v20.541C0 23.226.792 24 1.77 24h20.4525C23.2005 24 24 23.226 24 22.2705V1.7295C24 .774 23.2005 0 22.2225 0h.0015z"
                        fillRule="evenodd"
                    />
                </S.Svg>
            </S.Container>
        );
    } else if (site === 'vimeo') {
        return (
            <S.Container href={siteUrls.vimeo} target="noopener" title="Follow us">
                <S.Svg
                    height="56.693"
                    width="56.693"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 56.693 56.693"
                >
                    <path d="M3.602 22.974l2.01 2.636s4.146-3.267 5.528-1.634c1.382 1.634 6.656 21.357 8.417 24.997 1.537 3.192 5.777 7.413 10.426 4.397 4.646-3.014 20.098-16.205 22.863-31.781C55.61 6.015 34.252 9.276 31.992 22.845c5.652-3.395 8.671 1.38 5.778 6.784-2.889 5.399-5.527 8.921-6.908 8.921-1.379 0-2.441-3.612-4.021-9.928-1.635-6.53-1.624-18.291-8.416-16.958-6.406 1.257-14.823 11.31-14.823 11.31z" />
                </S.Svg>
            </S.Container>
        );
    }
};

export default SocialIcon;
