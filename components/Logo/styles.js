// @flow
import styled from '@emotion/styled';

export const SVG = styled.svg`
    fill: ${props => props.theme.logoFill};
    height: auto;
    width: 140px;
`;
