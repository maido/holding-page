// @flow
import * as React from 'react';
import * as S from './styles';

type Props = {
    children: React.Node,
    grow?: Boolean,
    isPageTheme?: boolean,
    overlap?: string,
    overlapSize?: number,
    theme?: string
};

const Theme = ({
    children,
    grow = false,
    isPageTheme = false,
    overlap,
    overlapSize = 60,
    theme,
    ...props
}: Props) => (
    <S.Container
        theme={theme}
        isPageTheme={isPageTheme}
        overlap={overlap}
        overlapSize={overlapSize}
        grow={grow}
        {...props}
    >
        {children}
    </S.Container>
);

export default Theme;
