// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {themes, transitions} from '../../globals/variables';

export const themeStyles = theme => css`
    ${themes[theme] &&
        `background-color: ${themes[theme].background};
        color: ${themes[theme].text};

        a:not([class]) {
            color: ${themes[theme].links};
            }`}
`;

export const Container = styled.div`
    transition: ${transitions.default};

    ${props => props.isPageTheme && `min-height: 100vh;`}

    ${props => props.grow && `flex-grow: 1;`}

    ${props =>
        props.overlap &&
        `position: relative;

            &::before {
                background-color: ${themes[props.overlap].background};
                content: '';
                left: 0;
                height: ${rem(props.overlapSize)};
                position: absolute;
                top: 0;
                width: 100%;
                z-index: 0;
            }
            &::after {
                background-color: ${themes[props.theme].background};
                border-radius: ${rem(30)};
                content: '';
                left: 0;
                height: ${rem(30)};
                overflow: hidden;
                position: absolute;
                top: ${rem(props.overlapSize - 30 / 2)};
                width: 100%;
                z-index: 0;
            }

        > div,
        > section {
            position: relative;
            z-index: 1;
        }
    `}

    ${props => themeStyles(props.theme)}
`;
