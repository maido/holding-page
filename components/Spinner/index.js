// @flow
import * as React from 'react';
import * as S from './styles';

const Spinner = () => (
    <S.Container>
        <div />
        <div />
        <div />
        <div />
    </S.Container>
);

export default Spinner;
