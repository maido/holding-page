// @flow
import React, {useEffect} from 'react';
import {ThemeProvider} from 'emotion-theming';
import {initGA, logPageView} from '../../globals/analytics';
import {themes} from '../../globals/variables';
import Container from '../Container';
import Theme from '../Theme';
import * as S from './styles';

type Props = {
    children: React.Node,
    theme?: string
};

const Layout = ({children, theme = 'secondary'}: Props) => {
    useEffect(() => {
        if (process.env.NODE_ENV === 'production') {
            if (typeof window !== 'undefined' && !window.GA_INITIALIZED) {
                initGA();
                window.GA_INITIALIZED = true;
            }

            logPageView();
        }
    }, []);

    return (
        <ThemeProvider theme={themes[theme]}>
            <Theme theme={theme} isPageTheme={true} css={S.container}>
                <S.Main>{children}</S.Main>
            </Theme>
        </ThemeProvider>
    );
};

export default Layout;
