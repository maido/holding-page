// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {responsiveRem} from '../../globals/functions';
import {spacing} from '../../globals/variables';

export const container = css`
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
`;

export const Main = styled.main`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
`;

export const footer = css`
    padding-bottom: ${responsiveRem(spacing.m)};
    padding-top: ${responsiveRem(spacing.m)};
`;
