// @flow
import React, {Fragment} from 'react';
import {ErrorMessage, Field} from 'formik';
import * as FormStyles from '../Form/styles';
import * as S from './styles';

type Props = {
    name: string,
    label: string,
    value?: string
};

const InputCheckbox = ({name, label, value, ...props}: Props) => (
    <Field name={name}>
        {({field, form}) => (
            <Fragment>
                <S.Container>
                    <S.Input
                        id={name}
                        type="checkbox"
                        name={name}
                        value={field.value}
                        checked={field.value}
                        onChange={field.onChange}
                        onBlur={field.onBlur}
                        {...props}
                    />
                    <S.Label htmlFor={name}>{label}</S.Label>
                </S.Container>
                <ErrorMessage component="span" name={name} css={FormStyles.error} />
            </Fragment>
        )}
    </Field>
);

export default InputCheckbox;
