// @flow
import styled from '@emotion/styled';
import {rem, rgba} from 'polished';
import {breakpoints, colors, spacing, transitions} from '../../globals/variables';

export const Container = styled.div`
    display: flex;
    position: relative;
`;

export const Input = styled.input`
    opacity: 0;
    position: absolute;
`;

export const Label = styled.label`
    align-items: center;
    display: flex;
    cursor: pointer;
    font-size: ${rem(14)};
    line-height: 1.5;
    padding: 0;
    position: relative;

    &::before {
        background-color: ${colors.white};
        border: 2px solid ${colors.purplishBlue};
        border-radius: ${rem(2)};
        content: '';
        display: inline-block;
        height: ${rem(20)};
        margin-right: ${rem(spacing.s)};
        transition: ${transitions.default};
        width: ${rem(20)};
    }

    ${Input}:hover + &::before {
        background-color: ${rgba(colors.purplishBlue, 0.25)};
    }
    ${Input}:focused + &::before {
        transform: scale(0.9);
    }
    ${Input}:checked + &::before {
        background-color: ${colors.purplishBlue};
    }
    ${Input}:checked + &::after {
        content: '';
        position: absolute;
        left: 5px;
        top: 50%;
        background: white;
        width: 2px;
        height: 2px;
        box-shadow: 2px 0 0 white, 4px 0 0 white, 4px -2px 0 white, 4px -4px 0 white,
            4px -6px 0 white, 4px -8px 0 white;
        transform: translateY(-50%) rotate(45deg);
    }
`;
