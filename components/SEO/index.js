// @flow
import React from 'react';
import Helmet from 'react-helmet';
import truncate from 'lodash/truncate';

type Props = {
    description?: string,
    lang?: string,
    image?: string,
    meta?: Array<{
        property: string,
        content: string
    }>,
    title?: string
};

const SEO = ({
    data = {},
    description = '',
    lang = 'en',
    image = '/static/og-image.jpg',
    meta = [],
    title = ''
}: Props) => {
    return (
        <Helmet
            htmlAttributes={{
                lang
            }}
            title={title}
            titleTemplate={`%s | Thought for Food`}
            meta={[
                {
                    name: 'description',
                    content: description
                },
                {
                    property: 'og:title',
                    content: title
                },
                {
                    property: 'og:description',
                    content: description
                },
                {
                    property: 'og:image',
                    content: image
                },
                {
                    property: 'og:type',
                    content: 'website'
                },
                {
                    name: 'twitter:card',
                    content: 'summary'
                },
                {
                    name: 'twitter:title',
                    content: title
                },
                {
                    name: 'twitter:description',
                    content: description
                }
            ]}
        >
            <link rel="apple-touch-icon" sizes="57x57" href="/static/apple-icon-57x57.png" />
            <link rel="apple-touch-icon" sizes="60x60" href="/static/apple-icon-60x60.png" />
            <link rel="apple-touch-icon" sizes="72x72" href="/static/apple-icon-72x72.png" />
            <link rel="apple-touch-icon" sizes="76x76" href="/static/apple-icon-76x76.png" />
            <link rel="apple-touch-icon" sizes="114x114" href="/static/apple-icon-114x114.png" />
            <link rel="apple-touch-icon" sizes="120x120" href="/static/apple-icon-120x120.png" />
            <link rel="apple-touch-icon" sizes="144x144" href="/static/apple-icon-144x144.png" />
            <link rel="apple-touch-icon" sizes="152x152" href="/static/apple-icon-152x152.png" />
            <link rel="apple-touch-icon" sizes="180x180" href="/static/apple-icon-180x180.png" />
            <link
                rel="icon"
                type="image/png"
                sizes="192x192"
                href="/static/android-icon-192x192.png"
            />
            <link rel="icon" type="image/png" sizes="32x32" href="/static/favicon-32x32.png" />
            <link rel="icon" type="image/png" sizes="96x96" href="/static/favicon-96x96.png" />
            <link rel="icon" type="image/png" sizes="16x16" href="/static/favicon-16x16.png" />
            <link rel="manifest" href="/static/manifest.json" />
            <meta name="msapplication-TileColor" content="#ffffff" />
            <meta name="msapplication-TileImage" content="/static/ms-icon-144x144.png" />
            <meta name="theme-color" content="#ffffff" />
        </Helmet>
    );
};

export default SEO;
