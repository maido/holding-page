// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import {Formik, Form} from 'formik';
import InputText from './';

const stories = storiesOf('InputText', module);

stories.add('default', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputText name="last_name" />
                </Form>
            )}
        />
    );
});

stories.add('with label', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputText name="last_name" label="Last name" />
                </Form>
            )}
        />
    );
});

stories.add('with placeholder', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputText name="last_name" placeholder="Doe" />
                </Form>
            )}
        />
    );
});

stories.add('as date', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputText name="last_name" type="date" />
                </Form>
            )}
        />
    );
});

stories.add('as email', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputText name="last_name" type="email" />
                </Form>
            )}
        />
    );
});

stories.add('as passsword', () => {
    return (
        <Formik
            initialValues={{password: 'foobarbaz1'}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputText name="password" type="password" />
                </Form>
            )}
        />
    );
});

stories.add('has error', () => {
    return (
        <Formik
            initialValues={{last_name: 1}}
            validate={{last_name: 1}}
            isInitialValid={false}
            validationSchema={accountSignUpStep1Schema}
            onSubmit={() => {}}
            render={props => (
                <Form>
                    <InputText name="last_name" label="Last name" placeholder="Doe" />
                </Form>
            )}
        />
    );
});

stories.add('has field disabled', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputText name="last_name" label="Last name" placeholder="Doe" />
                </Form>
            )}
        />
    );
});
