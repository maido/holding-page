// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {spacing} from '../../globals/variables';

export const Container = styled.div`
    position: relative;
`;

export const InputContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    position: relative;
`;

export const PasswordToggle = styled.button`
    position: absolute;
    right: ${rem(spacing.s)};
    top: 50%;
    transform: translateY(-50%);
`;
