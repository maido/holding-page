// @flow
import React, {Fragment, useState} from 'react';
import {ErrorMessage, Field} from 'formik';
import * as FormStyles from '../Form/styles';
import * as S from './styles';

type Props = {
    autoComplete?: boolean,
    label?: string,
    name: string,
    placeholder?: string,
    type?: string
};

const InputText = ({autoComplete, label, name, placeholder, type = 'text'}: Props) => {
    const [isPasswordVisible, setPasswordVisible] = useState(false);
    const [fieldType, setFieldType] = useState(type);

    const togglePasswordVisibility = () => {
        if (isPasswordVisible) {
            setFieldType('password');
            setPasswordVisible(false);
        } else {
            setFieldType('text');
            setPasswordVisible(true);
        }
    };

    return (
        <Fragment>
            <Field label={label} name={name} placeholder={placeholder}>
                {({field, form, ...props}) => (
                    <S.Container>
                        {label && (
                            <label htmlFor={name} css={FormStyles.label}>
                                {label}
                            </label>
                        )}
                        <S.InputContainer>
                            <input
                                css={[
                                    FormStyles.input,
                                    form.touched[field.name] && form.errors[field.name]
                                        ? FormStyles.inputError
                                        : null
                                ]}
                                id={name}
                                name={field.name}
                                type={fieldType}
                                autoComplete={type === 'password' ? 'off' : autoComplete}
                                placeholder={placeholder}
                                {...field}
                                {...props}
                            />
                            {type === 'password' && !isPasswordVisible && (
                                <S.PasswordToggle
                                    type="button"
                                    onClick={togglePasswordVisibility}
                                    aria-label="Show password"
                                    title="Show password"
                                >
                                    <svg width="18" height="12" xmlns="http://www.w3.org/2000/svg">
                                        <g fill="none" fillRule="evenodd">
                                            <path d="M-3-6h24v24H-3z" />
                                            <path
                                                d="M9 0C4.9091 0 1.41545 2.488 0 6c1.41545 3.512 4.9091 6 9 6s7.58455-2.488 9-6c-1.41545-3.512-4.9091-6-9-6zm0 10c-2.208 0-4-1.792-4-4s1.792-4 4-4 4 1.792 4 4-1.792 4-4 4zm0-6.5C7.61667 3.5 6.5 4.61667 6.5 6S7.61667 8.5 9 8.5s2.5-1.11667 2.5-2.5S10.38333 3.5 9 3.5z"
                                                fill="#32ebb4"
                                            />
                                        </g>
                                    </svg>
                                </S.PasswordToggle>
                            )}
                            {type === 'password' && isPasswordVisible && (
                                <S.PasswordToggle
                                    type="button"
                                    onClick={togglePasswordVisibility}
                                    aria-label="Title password"
                                    title="Title password"
                                >
                                    <svg width="18" height="12" xmlns="http://www.w3.org/2000/svg">
                                        <g fill="none" fillRule="evenodd">
                                            <path d="M-3-6h24v24H-3z" />
                                            <path
                                                d="M9 0C4.9091 0 1.41545 2.488 0 6c1.41545 3.512 4.9091 6 9 6s7.58455-2.488 9-6c-1.41545-3.512-4.9091-6-9-6zm0 10c-2.208 0-4-1.792-4-4s1.792-4 4-4 4 1.792 4 4-1.792 4-4 4zm0-6.5C7.61667 3.5 6.5 4.61667 6.5 6S7.61667 8.5 9 8.5s2.5-1.11667 2.5-2.5S10.38333 3.5 9 3.5z"
                                                fill="#32ebb4"
                                            />
                                        </g>
                                    </svg>
                                </S.PasswordToggle>
                            )}
                        </S.InputContainer>
                    </S.Container>
                )}
            </Field>
            <ErrorMessage component="span" name={name} css={FormStyles.error} />
        </Fragment>
    );
};

export default InputText;
