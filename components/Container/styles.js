// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {breakpoints, spacing} from '../../globals/variables';

export const CONTAINER_WIDTHS = {
    default: 1350,
    medium: 900,
    small: 700
};

export const containerCSS = props => css`
    box-sizing: border-box;
    display: block;
    margin-left: auto;
    margin-right: auto;
    max-width: ${rem(CONTAINER_WIDTHS[props.size])};
    padding: ${responsiveRem(spacing.m)} ${rem(spacing.m)};
    position: relative;
    width: 100%;

    ${props.paddingVertical === false && `padding-bottom: 0; padding-top: 0;`}
`;

export const Container = styled.div`
    ${containerCSS};
`;
