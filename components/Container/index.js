// @flow
import * as React from 'react';
import * as S from './styles';

type Props = {
    as?: string,
    children: React.Node,
    padding?: boolean,
    size?: string,
    style?: Object
};

const Container = ({
    as = 'div',
    children,
    paddingVertical = true,
    size = 'default',
    ...props
}: Props) => (
    <S.Container as={as} paddingVertical={paddingVertical} size={size} {...props}>
        {children}
    </S.Container>
);

export default Container;
