// @flow
import * as React from 'react';
import {useSpring, animated} from 'react-spring/web.cjs';

type Props = {
    children: React.Node,
    delay?: number,
    config?: Object,
    offset?: Object,
    watch?: boolean
};

const AnimateWhenVisible = ({
    children,
    delay = 0,
    config,
    offset = {top: 200},
    watch = true
}: Props) => {
    const defaultConfig = {
        config: {
            tension: 60,
            mass: 1,
            friction: 10
        },
        delay,
        from: {transform: `translateY(20px)`, opacity: 0},
        to: {transform: `translateY(0)`, opacity: 1}
    };
    const springConfig = {
        ...defaultConfig,
        ...config
    };

    const [styleProps, setStyleProps] = useSpring(() => ({
        config: springConfig.config,
        delay: springConfig.delay,
        from: springConfig.from
    }));

    setTimeout(() => setStyleProps(springConfig.to), delay);

    return <animated.div style={styleProps}>{children}</animated.div>;
};

export default AnimateWhenVisible;
