// @flow
import {css} from '@emotion/core';
import {between, rem} from 'polished';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    themes,
    transitions
} from '../../globals/variables';

export const label = css`
    display: block;
    font-family: ${fontFamilies.bold};
    font-size: ${rem(16)};
    font-weight: 700;
    line-height: 1.2;
    margin-bottom: ${rem(spacing.s)};

    @media (max-width: ${rem(breakpoints.mobile)}) {
        margin-top: ${rem(spacing.xs)};
    }
`;

export const input = css`
    appearance: none;
    border: 1px solid ${themes.inactiveFormField.border};
    border-radius: ${rem(2)};
    color: ${themes.activeFormField.text};
    font-family: ${fontFamilies.bold};
    font-size: ${rem(fontSizes.default)};
    line-height: 1.4;
    padding: ${rem(spacing.s)};
    transition: ${transitions.default};
    width: 100%;

    &::placeholder {
        color: ${themes.inactiveFormField.text};
    }

    &:focus {
        border-color: ${themes.activeFormField.border};
        color: ${themes.activeFormField.text};
        outline: none;
    }
`;

export const inputError = css`
    border-color: ${colors.red};
`;

export const error = css`
    color: ${colors.red};
    display: block;
    font-style: italic;
    line-height: 1.3;
    margin-top: ${rem(spacing.xs)};
`;
