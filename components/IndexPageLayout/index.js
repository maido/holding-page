// @flow
import React, {Fragment, useState} from 'react';
import {useSpring, animated} from 'react-spring/web.cjs';
import {object, boolean, string} from 'yup';
import {Formik, Form} from 'formik';
import AnimateWhenVisible from '../AnimateWhenVisible';
import CTAButton from '../CTAButton';
import FullScreenMessage from '../FullScreenMessage';
import InputCheckbox from '../InputCheckbox';
import InputText from '../InputText';
import PageIntro from '../PageIntro';
import SocialIcon, {siteUrls as socialSiteUrls} from '../SocialIcon';
import * as UI from '../UI/styles';
import * as S from './styles';

const formSchema = object().shape({
    NAME: string()
        .min(2, 'Please provide your  name')
        .max(255, 'Sorry, please shorten your name')
        .required('Please provide your  name'),
    LA_NAME: string()
        .min(2, 'Please provide your  name')
        .max(255, 'Sorry, please shorten your name')
        .required('Please provide your  name'),
    EMAIL: string()
        .email('Your email address is not valid')
        .required('Please provide your email address'),
    terms: boolean().oneOf([true], 'You must read and accept our T&Cs')
});

const IndexPageLayout = () => {
    const [formState, setFormState] = useState('default');
    const [formStep, setFormStep] = useState('default');
    const formAnimation = useSpring({
        config: {tension: 50, mass: 1, friction: 15},
        delay: 400,
        from: {transform: `translateY(20px)`, opacity: 0},
        to: {transform: `translateY(0)`, opacity: 1}
    });

    const handleFormSubmit = (values, handlers) => {
        setFormState('pending');

        fetch('/api/newsletter-signup', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(values)
        })
            .then(r => r.json())
            .then(response => {
                handlers.setSubmitting(false);

                if (response.errors) {
                    setFormState('default');
                    handlers.setErrors(response.errors);
                } else if (response.success) {
                    setFormState('success');
                    setTimeout(() => setFormStep('success'), 750);
                } else {
                    setFormState('default');
                }
            });
    };

    return (
        <Fragment>
            {formStep === 'success' && (
                <FullScreenMessage
                    title="Nice one!"
                    text="We’re still in the process of building the Digital Labs platform, we’ll email you when you can join."
                    image="/static/register-badge.png"
                    cta={{label: 'Find out more about TFF', url: 'http://thoughtforfood.org'}}
                />
            )}
            <S.Container>
                <S.Column css={UI.centerText}>
                    <PageIntro
                        image="/static/badge-rosette.png"
                        title="Where next-gen innovation happens"
                        text="Join the 2019 TFF Challenge, a global “collaborative competition” to develop and launch breakthrough solutions that sustainably feed the world."
                    />

                    <S.List>
                        {Object.keys(socialSiteUrls).map((site, index) => (
                            <S.ListItem size="xs" key={site}>
                                <AnimateWhenVisible delay={1000 + index * 50}>
                                    <SocialIcon site={site} />
                                </AnimateWhenVisible>
                            </S.ListItem>
                        ))}
                    </S.List>
                </S.Column>
                <S.Column theme="white">
                    <animated.div style={formAnimation}>
                        <UI.Heading3 as="h2" css={UI.centerText}>
                            Register your interest
                        </UI.Heading3>

                        <Formik
                            initialValues={{NAME: '', LA_NAME: '', EMAIL: '', terms: false}}
                            validationSchema={formSchema}
                            onSubmit={handleFormSubmit}
                            action="https://www.mailchimp.com"
                            render={() => (
                                <Form>
                                    <UI.LayoutContainer type="matrix">
                                        <UI.LayoutItem sizeAtMobile={6 / 12}>
                                            <InputText
                                                name="NAME"
                                                label="First name"
                                                placeholder="Jane"
                                            />
                                        </UI.LayoutItem>
                                        <UI.LayoutItem sizeAtMobile={6 / 12}>
                                            <InputText
                                                name="LA_NAME"
                                                label="Last name"
                                                placeholder="Doe"
                                            />
                                        </UI.LayoutItem>
                                        <UI.LayoutItem>
                                            <InputText
                                                name="EMAIL"
                                                label="Email"
                                                placeholder="janedoe@gmail.com"
                                                type="email"
                                            />
                                        </UI.LayoutItem>
                                        <UI.LayoutItem>
                                            <UI.Spacer sizeAtMobile="none" />

                                            <InputCheckbox
                                                name="terms"
                                                label="I agree that TFF sends me news and updates."
                                            />
                                        </UI.LayoutItem>
                                        <UI.LayoutItem>
                                            <UI.Spacer size="xs" />
                                            <CTAButton
                                                type="submit"
                                                disabled={formState !== 'default'}
                                                state={formState}
                                                block={true}
                                            >
                                                Continue
                                            </CTAButton>
                                        </UI.LayoutItem>
                                    </UI.LayoutContainer>
                                </Form>
                            )}
                        />
                    </animated.div>
                </S.Column>
            </S.Container>
        </Fragment>
    );
};

export default IndexPageLayout;
