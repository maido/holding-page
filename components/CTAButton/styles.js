// @flow
import {css, keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {rem, shade} from 'polished';
import {
    breakpoints,
    fontFamilies,
    fontSizes,
    spacing,
    themes,
    transitions
} from '../../globals/variables';

type ButtonThemeProps = {
    border: boolean,
    block: boolean,
    ghost: boolean,
    theme: string
};

export const button = css`
    border-radius: 0;
    border: 1px solid transparent;
    border-radius: ${rem(50)};
    cursor: pointer;
    display: inline-block;
    font-family: ${fontFamilies.bold};
    font-size: ${rem(fontSizes.default)};
    font-weight: 700;
    line-height: 1.3;
    overflow: hidden;
    opacity: 1;
    padding: ${rem(13)} ${rem(34)};
    position: relative;
    text-align: center;
    transition: ${transitions.default};
    vertical-align: middle;

    // svg {
    //     margin-left: ${rem(spacing.xs)};
    //     transition: ${transitions.default};
    // }
    // path {
    //     fill: currentColor;
    // }

    &:hover,
    &:focus {
        box-shadow: 0 10px 20px rgba(0, 0, 0, 0.04);
        transform: translateY(-1px);

        // svg {
        //     transform: translateX(2px);
        // }
    }

    &:active {
        outline: none;
        transform: scale(0.95);
    }

    &[aria-current='step'],
    &[aria-current='page'],
    &[disabled] {
        pointer-events: none;
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        & + & {
            margin-left: ${rem(spacing.xs)};
        }
    }
`;

export const buttonTheme = (props: ButtonThemeProps) => css`
    background-color: ${themes[props.theme].buttonBackground || themes[props.theme].background};
    border-color: ${themes[props.theme].background};
    color: ${themes[props.theme].buttonText || themes[props.theme].text};

    &:hover,
    &:focus {
        background-color: ${shade(
            0.05,
            themes[props.theme].buttonBackground || themes[props.theme].background
        )};
        border-color: ${shade(
            0.05,
            themes[props.theme].buttonBackground || themes[props.theme].background
        )};
        color: ${themes[props.theme].buttonText || themes[props.theme].text};
    }

    ${props.border && `border: 1px solid;`}

    ${props.block && `width: 100%;`}

    ${props.blockAtMobile &&
        `@media (max-width: ${rem(breakpoints.mobile)}) {
        display: block;
        width: 100%;

        & + & {
            margin-top: ${rem(spacing.xs)};
        }
    }`}

    ${!props.blockAtMobile &&
        `@media (max-width: ${rem(breakpoints.mobile)}) {
        padding-left: ${rem(spacing.m * 1.5)};
        padding-right: ${rem(spacing.m * 1.5)};
    }`}

    ${props.wide && `padding-left: ${rem(spacing.xl)}; padding-right: ${rem(spacing.xl)};`}

    ${props.size &&
        props.size === 'small' &&
        `font-size: ${rem(14)};padding: ${rem(10)} ${rem(30)};`}

    ${props.ghost &&
        `&:not(:hover):not(:active) {
        background: transparent;
        border: 1px solid ${themes[props.theme].background};
        color: ${themes[props.theme].background};
    }`}

    ${props.basic &&
        `&,&:hover,&:focus {
        background-color: transparent;
        border: 0;
        box-shadow: none;
        font-weight: 700;
        padding: ${rem(spacing.s)};
        transform: none;
    }`}
`;

export const Label = styled.span`
    opacity: 1;

    ${props => (props.isHidden ? `opacity: 0; transition: ${transitions.default};` : '')};
`;

export const SpinnerContainer = styled.div`
    left: 50%;
    position: absolute;
    top: 54%;
    transform: translateX(-50%) translateY(-50%);
`;

const iconAnimation = keyframes`
    from {
        opacity: 0;
        transform: scale(0) translateX(-50%) translateY(-50%);
    }
    to {
        opacity: 1;
        transform: scale(1) translateX(-50%) translateY(-50%);
    }
`;

export const Icon = styled.svg`
    animation: ${iconAnimation} 0.4s ease-in-out;
    height: ${rem(20)};
    left: 50%;
    position: absolute;
    top: 50%;
    transform: translateX(-50%) translateY(-50%);
    transform-origin: center center;
    width: auto;

    path {
        fill: currentColor;
    }
`;
