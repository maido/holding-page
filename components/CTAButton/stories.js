// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import {withKnobs, boolean, object, select, text} from '@storybook/addon-knobs';
import CTAButton from './';

const sizeOptions = {
    Default: 'default',
    Small: 'small'
};
const stateOptions = {
    Default: 'default',
    Pending: 'pending',
    Success: 'success',
    Error: 'error'
};
const themeOptions = {
    Primary: 'primary',
    Secondary: 'secondary',
    Tertiary: 'tertiary',
    White: 'white'
};
const stories = storiesOf('CTAButton', module);

stories.addDecorator(withKnobs);

stories.add('default', () => {
    return <CTAButton>Click here</CTAButton>;
});

stories.add('with theme', () => {
    return <CTAButton theme="secondary">Click here</CTAButton>;
});

stories.add('with ghost modifier', () => {
    return <CTAButton ghost={true}>Click here</CTAButton>;
});

stories.add('with wide modifier', () => {
    return <CTAButton wide={true}>Click here</CTAButton>;
});

stories.add('with block modifier', () => {
    return <CTAButton block={true}>Click here</CTAButton>;
});

stories.add('with size modifier `small`', () => {
    return <CTAButton size="small">Click here</CTAButton>;
});

stories.add('with basic modifier', () => {
    return <CTAButton basic={true}>Click here</CTAButton>;
});

stories.add('as internal link', () => {
    return <CTAButton href="/">Click here</CTAButton>;
});

stories.add('as external link', () => {
    return <CTAButton href="http://www.google.com">Visit our partner</CTAButton>;
});

stories.add('as button', () => {
    return (
        <CTAButton type="button" onClick={() => alert('hi')}>
            Submit
        </CTAButton>
    );
});

stories.add('with active state', () => {
    return (
        <CTAButton type="button" onClick={() => alert('hi')} aria-current="step">
            Submit
        </CTAButton>
    );
});

stories.add('with pending state', () => {
    return <CTAButton state="pending">Submit</CTAButton>;
});

stories.add('with success state', () => {
    return <CTAButton state="success">Submit</CTAButton>;
});

stories.add('playground', () => {
    return (
        <CTAButton
            block={boolean('Block', false)}
            border={boolean('Border', false)}
            ghost={boolean('Ghost', false)}
            wide={boolean('Wide', false)}
            basic={boolean('Basic', false)}
            state={select('State', stateOptions, 'default')}
            theme={select('Theme', themeOptions, 'primary')}
            size={select('Size', sizeOptions, 'default')}
        >
            {text('Label', 'Click here')}
        </CTAButton>
    );
});
