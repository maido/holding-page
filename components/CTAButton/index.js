// @flow
import * as React from 'react';
import Link from '../Link';
import Spinner from '../Spinner';
import * as S from './styles';

type Props = {
    basic?: boolean,
    block?: boolean,
    blockAtMobile?: boolean,
    border?: boolean,
    children: React.Node,
    ghost?: boolean,
    href?: string,
    state?: 'default' | 'pending' | 'success',
    size?: string,
    theme?: string,
    type?: string,
    wide?: boolean
};

const CTAButton = ({
    basic = false,
    block = false,
    blockAtMobile = true,
    border = false,
    children,
    ghost = false,
    href = '',
    state = 'default',
    size = 'default',
    theme = 'primary',
    type = 'button',
    wide = false,
    ...props
}: Props) => (
    <Link
        href={href}
        css={[
            S.button,
            S.buttonTheme({basic, block, blockAtMobile, border, ghost, size, theme, wide})
        ]}
        type={type}
        {...props}
    >
        {state === 'pending' && (
            <S.SpinnerContainer>
                <Spinner />
            </S.SpinnerContainer>
        )}
        {state === 'success' && (
            <S.Icon height="15" width="18" viewBox="0 0 15 18" xmlns="http://www.w3.org/2000/svg">
                <path d="M6 10.7L1.8 6.5.4 7.9 6 13.5l12-12L16.6.1 6 10.7z" fillRule="evenodd" />
            </S.Icon>
        )}
        <S.Label isHidden={state !== 'default'}>{children}</S.Label>
    </Link>
);

export default CTAButton;
