// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import {withKnobs, object, select, text} from '@storybook/addon-knobs';
import FullScreenMessage from '.';
import Theme from '../Theme';

const themeOptions = {
    Primary: 'primary',
    Secondary: 'secondary',
    Tertiary: 'tertiary',
    White: 'white'
};
const stories = storiesOf('FullScreenMessage', module);

stories.addDecorator(withKnobs);

stories.add('default', () => {
    return (
        <FullScreenMessage
            title="Account created"
            text="Your account was successfully created."
            cta={{label: 'Go to dashboard', url: '/'}}
        />
    );
});

stories.add('playground', () => {
    return (
        <FullScreenMessage
            title={text('Title', 'Account created')}
            theme={select('Theme', themeOptions, 'secondary')}
            text={text('Text', 'Your account was successfully created.')}
            image={text('Image', '')}
            cta={object('CTA', {label: 'Go to dashboard', url: '/'})}
        />
    );
});
