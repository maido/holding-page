// @flow
import React from 'react';
import {useSpring, animated} from 'react-spring/web.cjs';
import Container from '../Container';
import CTAButton from '../CTAButton';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    cta: CTA,
    image?: string,
    text: string,
    theme?: string,
    title: string
};

const FullScreenMessage = ({cta, image, text, theme = 'secondary', title}: Props) => {
    const containerAnimation = useSpring({
        config: {tension: 60, mass: 1, friction: 10},
        from: {opacity: 0},
        to: {opacity: 1}
    });
    const badgeAnimation = useSpring({
        config: {tension: 200, mass: 5, friction: 30},
        delay: 100,
        from: {transform: `scale(0)`, opacity: 1},
        to: {transform: `scale(1)`, opacity: 1}
    });
    const textAnimation = useSpring({
        config: {tension: 60, mass: 1, friction: 10},
        delay: 500,
        from: {transform: `translateY(20px)`, opacity: 0},
        to: {transform: `translateY(0)`, opacity: 1}
    });
    const buttonAnimation = useSpring({
        config: {tension: 60, mass: 1, friction: 10},
        delay: 700,
        from: {transform: `translateY(20px)`, opacity: 0},
        to: {transform: `translateY(0)`, opacity: 1}
    });

    return (
        <animated.div style={containerAnimation} css={S.container(theme)}>
            <S.Wrapper>
                <S.ContentContainer>
                    {image && (
                        <animated.img style={badgeAnimation} src={image} alt="" css={S.badge} />
                    )}

                    <animated.div style={textAnimation}>
                        <UI.Heading2 as="h1" style={{marginBottom: 6}}>
                            {title}
                        </UI.Heading2>
                        <S.IntroText>{text}</S.IntroText>
                    </animated.div>
                </S.ContentContainer>

                {cta && (
                    <S.CTAContainer>
                        <animated.div style={textAnimation}>
                            <animated.div style={buttonAnimation}>
                                <CTAButton href={cta.url}>{cta.label}</CTAButton>
                            </animated.div>
                        </animated.div>
                    </S.CTAContainer>
                )}
            </S.Wrapper>
        </animated.div>
    );
};

export default FullScreenMessage;
