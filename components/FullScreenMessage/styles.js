// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {themeStyles} from '../Theme/styles';
import {breakpoints, spacing} from '../../globals/variables';

export const container = theme => css`
    ${themeStyles(theme)};
    align-items: center;
    bottom: 0;
    display: flex;
    height: 100%;
    left: 0;
    justify-content: center;
    position: fixed;
    padding: ${rem(spacing.m)};
    right: 0;
    text-align: center;
    top: 0;
    width: 100%;
    z-index: 100;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        padding: ${rem(spacing.l)};
    }
`;

export const badge = css`
    height: ${rem(140)};
    margin-bottom: ${rem(spacing.xs)};
    margin-left: auto;
    margin-right: auto;
    width: auto;
`;

export const IntroText = styled.p`
    margin-left: auto;
    margin-right: auto;
    max-width: ${rem(500)};
`;

export const Wrapper = styled.div`
    @media (max-width: ${rem(breakpoints.mobile)}) {
        display: flex;
        flex-direction: column;
        min-height: 100vh;
    }
`;

export const ContentContainer = styled.div`
    @media (max-width: ${rem(breakpoints.mobile)}) {
        display: flex;
        flex-grow: 1;
        flex-direction: column;
        justify-content: center;
    }
`;

export const CTAContainer = styled.div`
    padding-bottom: ${rem(spacing.m)};
    padding-top: ${rem(spacing.m)};
`;
