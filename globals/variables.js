// @flow
export const breakpoints = {
    mobileSmall: 540,
    mobile: 741,
    tabletSmall: 769,
    tablet: 1025,
    desktop: 1300,
    desktopLarge: 1800
};

export const colors = {
    white: '#fff',
    black: '#000',
    grey: '#d1d1d6',
    greyLight: '#f1f2f5',
    greyLightest: '#f6f7fa',
    greyMid: '#8e94a4',
    greyDark: '#8e8e93',
    blueGrey: '#e4e5e7',
    red: '#ff3b30',
    offWhite: '#fffde5',
    aquaMarine: '#1de8b5',
    lightAqua: '#8efadf',
    weirdGreen: '#4cd964',
    purplishBlue: '#651fff',
    brightSkyBlue: '#03c2f1',
    darkBlueGrey: '#1e2a4a',
    darkGreyBlue: '#2c3f6c',
    mediumPink: '#ff5d9e',
    neonBlue: '#00ebff',
    violet: '#F0E9FF',

    primary: '#1de8b5',
    secondary: '#1e2a4a',
    tertiary: '#e4e5e7'
};

export const themes = {
    primary: {
        background: colors.aquaMarine,
        logoFill: colors.offWhite,
        links: colors.purplishBlue,
        text: colors.darkBlueGrey
    },
    secondary: {
        background: colors.darkGreyBlue,
        logoFill: colors.offWhite,
        text: colors.offWhite
    },
    tertiary: {
        accent: colors.grey,
        background: colors.greyLight,
        links: colors.purplishBlue,
        logoFill: colors.purplishBlue,
        text: colors.darkBlueGrey
    },
    white: {
        background: colors.white,
        buttonText: colors.purplishBlue,
        links: colors.darkGreyBlue,
        logoFill: colors.darkBlueGrey,
        placeholder: colors.grey,
        text: colors.darkBlueGrey
    },
    brightSkyBlue: {
        background: colors.brightSkyBlue,
        buttonBackground: colors.white,
        buttonText: colors.darkBlueGrey,
        links: colors.offWhite,
        logoFill: colors.offWhite,
        placeholder: colors.grey,
        text: colors.offWhite
    },
    inactiveFormField: {
        background: colors.white,
        border: colors.grey,
        text: colors.greyDark
    },
    activeFormField: {
        background: colors.greyLightest,
        border: colors.greyMid,
        text: colors.darkBlueGrey
    },
    selectedFormField: {
        background: colors.aquaMarine,
        border: colors.greyMid,
        text: colors.darkBlueGrey
    },
    inactiveButton: {
        background: colors.blueGrey,
        text: colors.greyDark
    },
    activeButton: {
        background: colors.aquaMarine,
        text: colors.darkBlueGrey
    },
    modal: {
        background: colors.darkBlueGrey,
        text: colors.darkBlueGrey
    }
};

export const fontFamilies = {
    default: 'museo-sans',
    bold: 'museo-sans',
    italic: 'museo-sans',
    heading: 'museo-sans'
};

export const fontSizes = {
    default: 16,
    lead: 18,
    h1: 48,
    h2: 36,
    h3: 28,
    h4: 22,
    h5: 18,
    h6: 12
};

export const radius = 0;

export const shadows = {
    default: '0 6px 18px 0 rgba(0, 0, 0, 0.06)',
    hover: '0 6px 24px 0 rgba(0, 0, 0, 0.1)'
};

export const spacing = {
    xs: 6,
    s: 12,
    m: 24,
    l: 48,
    xl: 72,
    none: 0
};

export const transitions = {
    default: 'all .2s ease-in-out',
    slow: 'all .3s ease-out',
    bezier: 'all 2s cubic-bezier(.23,1,.32,1)'
};
