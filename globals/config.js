const WHITELISTED_ROUTES = [
    '/',
    '/login',
    '/forgot-password',
    '/register',
    '/terms-and-conditions',
    '/privacy-policy',
    '/cookie-policy',
    '/contact',
    '/pitch'
];

module.exports = {
    WHITELISTED_ROUTES
};
