// @flow
import {css} from '@emotion/core';
import {rem} from 'polished';
import {breakpoints, colors, fontFamilies, spacing, transitions} from './variables';

export const styles = css`
    :root {
        --responsive-spacing: 0.1px;
        --reach-menu-button: 1;
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        :root {
            --responsive-spacing: ${rem(spacing.xs)};
        }
    }

    @media (min-width: ${rem(breakpoints.desktop)}) {
        :root {
            --responsive-spacing: ${rem(spacing.s)};
        }
    }

    *,
    *::before,
    *::after {
        box-sizing: inherit;
    }

    html {
        background-color: ${colors.white};
        box-sizing: border-box;
        color: ${colors.black};
        font-family: ${fontFamilies.default};
        font-style: normal;
        font-weight: 300;
        overflow-x: hidden;
        -moz-osx-font-smoothing: grayscale;
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: 100%;
    }

    body {
        font-size: 1em;
        line-height: 1.7;
        margin: 0;
        max-width: 100%;
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        body {
            font-size: 0.95em;
        }
    }

    button {
        background: none;
        border: 0;
        cursor: pointer;
        outline: none;
        padding: 0;
    }

    i,
    em,
    b,
    strong {
        font-family: ${fontFamilies.default};
    }

    img {
        max-width: 100%;
    }

    ::selection {
        background-color: ${colors.primary};
        color: ${colors.white};
    }

    a,
    button {
        color: inherit;
        font-family: inherit;
        font-size: inherit;
        text-decoration: none;
        transition: ${transitions.default};
    }

    a {
        color: ${colors.primary};
    }
    a:hover,
    a:focus {
        color: ${colors.greyDark};
    }

    p {
        margin: 0;
    }

    p + * {
        margin-top: ${rem(spacing.m)};
    }

    @media (prefers-reduced-motion: reduce) {
        * {
            animation: none !important;
        }
    }
`;
