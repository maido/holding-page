// @flow
import {rem} from 'polished';
import cookie from 'cookie';

export const responsiveRem = (unit: number = 0, calculation: string = '+') =>
    `calc(${rem(unit)} ${calculation} var(--responsive-spacing))`;

export const bytesToSize = (bytes: number, seperator: string = '') => {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];

    if (bytes !== 0) {
        const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10);

        if (i === 0) {
            return `${bytes}${seperator}${sizes[i]}`;
        } else {
            return `${(bytes / 1024 ** i).toFixed(1)}${seperator}${sizes[i]}`;
        }
    }
};

export const parseCookies = (req: Object) => {
    let _cookie;

    if (req) {
        _cookie = req.headers.cookie || '';
    } else if (document) {
        _cookie = document.cookie;
    }

    return cookie.parse(_cookie);
};

export const getSsrQueryParams = (query = {}, params = {}, keys: Array<string> = []): Object => {
    const ssrParams = {};

    keys.map(key => {
        if (query[key]) {
            ssrParams[key] = query[key];
        } else if (params[key]) {
            ssrParams[key] = params[key];
        } else {
            ssrParams[key] = '';
        }
    });

    return ssrParams;
};
