type CTA = {
    url?: string,
    label: string,
    onClick?: Function
};

type TeamBadge = {
    title: string,
    photo: string,
    isProfileComplete?: boolean,
    members?: Array<{
        name: string,
        photo: string
    }>
};

type CourseCard = {
    description?: string,
    image?: string,
    index?: string,
    isNew?: boolean,
    level?: string,
    modules: number,
    moduleType?: string,
    progress: number,
    size?: string,
    slug: string,
    status?: string,
    title: string,
    url?: string
};
