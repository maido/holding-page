declare module '@emotion/core' {
    declare function Global (styles: Object | Array<Object>): string;

    declare function keyframes (...args: *): string;

    declare function css (...args: *): string;
};

declare module '@emotion/styled' {
    declare function styled (styles: Object | Array<Object>): string;

    declare export default typeof styled;
};
