// @flow
import Mailchimp from 'mailchimp-api-v3';

export default (req, res) => {
    const mailchimp = new Mailchimp(process.env.NEXT_STATIC_MAILCHIMP_API_KEY);

    if (req.method === 'POST') {
        mailchimp.request(
            {
                method: 'post',
                path: `/lists/${process.env.NEXT_STATIC_MAILCHIMP_LIST_ID}/members`,
                body: {
                    merge_fields: {LA_NAME: req.body.LA_NAME, NAME: req.body.NAME},
                    email_address: req.body.EMAIL,
                    status: 'subscribed'
                }
            },
            (error, response) => {
                res.setHeader('Content-Type', 'application/json');
                res.statusCode = 200;

                if (error) {
                    if (error.title === 'Member Exists') {
                        res.end(
                            JSON.stringify({
                                errors: {EMAIL: 'Sorry, that email has already subscribed.'}
                            })
                        );
                    } else {
                        res.end(
                            JSON.stringify({errors: {EMAIL: `MailChimp Error: ${error.detail}`}})
                        );
                    }
                } else {
                    res.end(JSON.stringify({success: response}));
                }
            }
        );
    } else {
        res.setHeader('Content-Type', 'application/json');
        res.statusCode = 500;
        res.end();
    }
};
