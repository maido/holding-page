// @flow
import React from 'react';
import Document, {Html, Head, Main, NextScript} from 'next/document';
import {Global} from '@emotion/core';
import {styles} from '../globals/styles';

class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx);

        return {...initialProps};
    }

    render() {
        return (
            <Html>
                <Head>
                    <Global styles={styles} />
                    <link rel="stylesheet" href="https://use.typekit.net/avg5qsl.css" />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}

export default MyDocument;
