// @flow
import React from 'react';
import Layout from '../components/Layout';
import IndexPageLayout from '../components/IndexPageLayout';
import SEO from '../components/SEO';

const IndexPage = () => (
    <Layout>
        <SEO
            title="Where next-gen innovation happens"
            description="A global “collaborative competition” to launch promising new ideas that address how we will feed and nourish our growing global population."
        />
        <IndexPageLayout />
    </Layout>
);

export default IndexPage;
